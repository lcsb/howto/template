document.addEventListener("DOMContentLoaded", () => {
  const btn = document.getElementById("back-top-button");

  window.addEventListener("scroll", () => {
    if (window.scrollY > 300) {
      btn.classList.add("active");
    } else {
      btn.classList.remove("active");
    }
  });

  btn.addEventListener("click", () => {
    window.scrollTo({
      top: 0,
      behavior: "smooth"
    });
  });
});
