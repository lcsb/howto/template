
# Static site template for `reploy`

Use this template as a base for creating new sites deployed by `reploy`.

Structure:
- directory `assets` is copied to HTTP root as is (`reploy` parameter `-a`)
- directory `templates` is used with `reploy` parameter `-t`

To use this, simply modify whatever you do not like in the template, most
notably the header information and footer license.

Ideally, you also want to keep some connection to this original template (fork
relation, remote, etc.), which helps with ingesting the updates created in this
repo. In the future, all files will be kept in directories `assets` and
`templates` as is now, to avoid conflicts with other repositories, allowing
also multi-origin merged repositories
